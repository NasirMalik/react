'use strict';

var counter = 0;
const e = React.createElement;

function updateCounter() {
  counter++;
  document.getElementById('counterDiv').innerHTML = counter + ' Likes';
}

function Hello() {
  return e('div', null, 
    e('h1', null, 'Say Hello'),
    e('button', { 
        onClick : () => {
          updateCounter();
        }
      }, 'Like It!' ),
      'Just Text'
  );
}


ReactDOM.render(
  e(Hello, null), 
    document.querySelector('#buttonDiv')
);

