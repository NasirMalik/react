// import React, { useState } from 'react';

'use strict';

var counter = 0;
const e = React.createElement;
const domContainer = document.querySelector('#buttonDiv');

class LikeButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = { counter };
  }

  render() {
    // if (this.state.liked) {
    //   return 'You liked this.';
    // }

    return e(
      'button',
      { 
        onClick: () => {
          alert(this.state.counter++);
          this.setState({ counter: counter+1 }); 
        }
      },
      this.state.counter
    );

  }
}

ReactDOM.render(e(LikeButton), domContainer);

