const jsDiv = document.getElementById('dateJs');
const reactDiv = document.getElementById('dateReact');

const render = () => {
    jsDiv.innerHTML = `
            <div class="split" >
                JS Tempplate
                <input />
                <p>${new Date()}</p>
            </div>
        `;

    var dynamicRaactDiv = React.createElement(
        "div",
        { className: "split" },
        "React Template ",
        React.createElement("input"),
        React.createElement(
            "p", null, new Date().toString()
        )
    ); 

    ReactDOM.render(dynamicRaactDiv, reactDiv);
}

// setInterval(render, 1000);