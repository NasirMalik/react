const fillIt = () => {
  let random = Math.random(); 
  let newDiv = `
    <div>
      ${random}
    </div>
  `;
  document.getElementById('emptyDiv').innerHTML = newDiv;
}

const fetchData = () => {
  fetch('https://api.github.com')
    .then(response => response.json())
    .then(data => document.getElementById('reponseDiv').innerHTML = data.authorizations_url)    
};

const fetchDataAsync = async () => {
  const response = await fetch('https://api.github.com')
  const data = await response.json();
  document.getElementById('reponseDiv').innerHTML = data.current_user_authorizations_html_url;    
};

// setInterval(fillIt, 1000);
// setInterval(fetchData, 2000);